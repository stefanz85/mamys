/*
 * Manage My Shows (MaMyS)
 * Copyright (C) 2014  Stefan Zimmermann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.afno.mamys.show.storage;

import java.util.Set;

import de.afno.mamys.model.Episode;
import de.afno.mamys.model.Season;
import de.afno.mamys.model.Show;

public interface ShowStorage {
	
	
	/**
	 * Add a show to the persistence.
	 * @param show
	 * @return
	 */
	public boolean add(Show show);
		
	/**
	 * Add a season for the given show to the persistence. If show is not yet in the persistence 
	 * layer, it will be stored as well.
	 * @param show
	 * @param season
	 * @return
	 */
	public boolean add(Show show, Season season);
	
	/**
	 * As above but stores multiple seasons for the given show.
	 * @param show
	 * @param seasons
	 * @return
	 */
	public boolean add(Show show, Set<Season> seasons);
	
	/**
	 * Adds an episode for a given season to the persistence. If the season is not yet in the 
	 * persistence layer, it will be stored as well.
	 * @param season
	 * @param episode
	 * @return
	 */
	public boolean add(Season season, Episode episode);
	
	/**
	 * As above but stores multiple episodes for the given season.
	 * @param season
	 * @param episodes
	 * @return
	 */
	public boolean add(Season season, Set<Episode> episodes);
	
	/**
	 * Remove a given show from the persistence layer. This will automatically remove all of it's
	 * seasons and episodes as well.
	 * @param show
	 * @return
	 */
	public boolean delete(Show show);
	
	/**
	 * Remove a given season from the persistence layer. This will automatically remove all of it's
	 * episodes as well.
	 * @param season
	 * @return
	 */
	public boolean delete(Season season);

	/**
	 * Remove a given episode from the persistence layer.
	 * @param episode
	 * @return
	 */
	public boolean delete(Episode episode);

	/**
	 * Get a set with all shows in the persistence layer.
	 * @return
	 */
	public Set<Show> getShows();
	
	/**
	 * Get a set with all seasons for a given show in the persistence layer.
	 * @param show
	 * @return
	 */
	public Set<Season> getSeasons(Show show);
	
	/**
	 * Get a set with all episodes for a given season in the persistence layer.
	 * @param season
	 * @return
	 */
	public Set<Episode> getEpisodes(Season season);
	
}
