package de.afno.mamys.show.manager.impl;

import java.util.Set;

import de.afno.mamys.model.Episode;
import de.afno.mamys.model.Marker;
import de.afno.mamys.model.Season;
import de.afno.mamys.model.Show;
import de.afno.mamys.show.manager.ShowManager;

public class ShowManagerImpl implements ShowManager {

	@Override
	public Set<Show> searchFromProvider(String searchString) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean addShow(Show show) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteShow(Show show) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateShow(Show show) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Set<Show> updateShows() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Show> listShows() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Season> listSeasons(Show show) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Episode> listEpisodes(Season season) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean markEpisode(Episode episode, Marker marker) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean markSeason(Season season, Marker marker) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean markShow(Show show, Marker marker) {
		// TODO Auto-generated method stub
		return false;
	}

}
