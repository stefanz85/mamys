/*
 * Manage My Shows (MaMyS)
 * Copyright (C) 2014  Stefan Zimmermann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.afno.mamys.show.manager;

import java.util.Set;

import de.afno.mamys.model.Episode;
import de.afno.mamys.model.Marker;
import de.afno.mamys.model.Season;
import de.afno.mamys.model.Show;

public interface ShowManager {

	public Set<Show> searchFromProvider(String searchString);
	
	public boolean addShow(Show show);
	
	public boolean deleteShow(Show show);
	
	public boolean updateShow(Show show);
	
	/**
	 * Update all shows. 
	 * TBD: Could return a list of updated Shows and perhaps additional information about how many
	 * updates were done, etc... how many new episodes or something like that?
	 * @return
	 */
	public Set<Show> updateShows();
	
	public Set<Show> listShows();
	
	public Set<Season> listSeasons(Show show);
	
	public Set<Episode> listEpisodes(Season season);
	
	/**
	 * Mark an episode with the given marker, like SEEN or AVAILABLE.
	 * @param episode
	 * @param marker
	 * @return
	 */
	public boolean markEpisode(Episode episode, Marker marker);
	
	public boolean markSeason(Season season, Marker marker);
	
	public boolean markShow(Show show, Marker marker);	
}
