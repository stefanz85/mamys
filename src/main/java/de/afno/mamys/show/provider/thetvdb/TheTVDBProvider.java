/*
 * Manage My Shows (MaMyS)
 * Copyright (C) 2014  Stefan Zimmermann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.afno.mamys.show.provider.thetvdb;

import java.util.Set;

import de.afno.mamys.model.Show;
import de.afno.mamys.show.provider.ShowProvider;

public class TheTVDBProvider implements ShowProvider {

	private static String APIKEY = null;
	
	@Override
	public Set<Show> search(String searchString) {
		// TODO Search directly at TheTVDB and return a list of shows
		return null;
	}

	@Override
	public Show get(Show show) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Show update(Show show) {
		// TODO Auto-generated method stub
		return null;
	}

}
