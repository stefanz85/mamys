/*
 * Manage My Shows (MaMyS)
 * Copyright (C) 2014  Stefan Zimmermann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.afno.mamys.show.provider;

import java.util.Set;

import de.afno.mamys.model.Show;

public interface ShowProvider {	
	
	/**
	 * Search the available shows at the provider for the given string in the show's title. 
	 * @param searchString
	 * @return
	 */
	public Set<Show> search(String searchString);
	
	/**
	 * Download all details about the given show from the provider. This contains also data for
	 * seasons and episodes.
	 * 
	 * @param show
	 * @return
	 */
	public Show get(Show show);
	
	/**
	 * Update the data for the given show. This contains also data for all seasons and episodes.
	 * @param show
	 * @return
	 */
	public Show update(Show show);
}
