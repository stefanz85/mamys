package de.afno.mamys.show.provider.thetvdb;

import java.net.URL;

public interface TVDBConnector {

	public StringBuffer request(URL url);
	
	public void setProxySettings(String proxy, String username, String password);
}
