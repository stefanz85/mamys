/*
 * Manage My Shows (MaMyS) Copyright (C) 2014 Stefan Zimmermann
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package de.afno.mamys;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import de.afno.mamys.show.manager.ShowManager;

public class MaMyS {

  private ShowManager showManager;

  public static void main(String[] args) {

    ApplicationContext context = new ClassPathXmlApplicationContext("spring-default.xml");
    // TODO Create a new ShowManager Object from a Spring Context
    // Prepare a REST API to provide ShowManager Functionality via Rest

    MaMyS mamys = context.getBean("mamys", MaMyS.class);

  }

  public MaMyS(ShowManager showManager) {
    this.showManager = showManager;
  }

}
